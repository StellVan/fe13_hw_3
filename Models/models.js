const mongoose = require('mongoose');

module.exports = {
  userModel: mongoose.model('user', {
    email: {
      required: true,
      unique: true,
      type: String,
    },
    role: {
      required: true,
      type: String,
    },
    created_date: {
      required: true,
      type: String,
    },
  }),
  credentialModel: mongoose.model('credential', {
    email: {
      required: true,
      unique: true,
      type: String,
    },
    password: {
      required: true,
      type: String,
    },
  }),
  loadModel: mongoose.model('load', {
    created_by: {
      required: true,
      type: String,
    },
    assigned_to: {
      required: true,
      type: String,
    },
    assigned_to_truck: {
      required: true,
      type: String,
    },
    status: {
      required: true,
      type: String,
    },
    state: {
      required: true,
      type: String,
    },
    name: {
      required: true,
      type: String,
    },
    payload: {
      required: true,
      type: Number,
    },
    pickup_address: {
      required: true,
      type: String,
    },
    delivery_address: {
      required: true,
      type: String,
    },
    dimensions: {
      width: {
        required: true,
        type: Number,
      },
      length: {
        required: true,
        type: Number,
      },
      height: {
        required: true,
        type: Number,
      },
    },
    logs: {
      required: true,
      type: Array,
    },
    created_date: {
      required: true,
      type: String,
    },
  }),
  truckModel: mongoose.model('truck', {
    created_by: {
      required: true,
      type: String,
    },
    assigned_to: {
      required: true,
      type: String,
    },
    assigned_to_load: {
      required: true,
      type: String,
    },
    status: {
      required: true,
      type: String,
    },
    state: {
      required: true,
      type: String,
    },
    type: {
      required: true,
      type: String,
    },
    created_date: {
      required: true,
      type: String,
    },
  }),
};
