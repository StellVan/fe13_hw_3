const { app } = require('./App/server');
const { port } = require('./config/config');

app.listen(port, () => {
  console.log(`Server is live on localhost:${port}/`);
});
