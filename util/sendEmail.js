const { email, emailPassword } = require("../config/config");
const nodemailer = require("nodemailer");

module.exports.sendEmail = (res, userEmail, subject, text, resMessage) => {
  const sender = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: email,
      pass: emailPassword,
    },
    tls: {
      rejectUnauthorized: false,
    },
  });

  const mailConfig = {
    from: email,
    to: userEmail,
    subject: subject,
    text: text,
  };

  sender.sendMail(mailConfig, (err) => {
    if (err) {
      return res.status(500).json({ message: err });
    } else {
      return res.json(resMessage);
    }
  });
};
