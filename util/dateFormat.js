let formatDate = (value) => {
  return value > 9 ? value : '0' + value;
};
module.exports.getFormatedDate = () => {
  let date = new Date();
  let dateString = `${formatDate(date.getHours())}:${formatDate(
    date.getMinutes()
  )} ${formatDate(date.getDate())}/${formatDate(
    date.getMonth() + 1
  )}/${date.getFullYear()}`;

  return dateString;
};
