module.exports.isShipper = (req, res, next) => {
  if (req.user.role === 'SHIPPER') {
    next();
  } else {
    return res.status(400).json({ message: 'You dont have shipper rights' });
  }
};
