module.exports.isDriver = (req, res, next) => {
  if (req.user.role === 'DRIVER') {
    next();
  } else {
    return res.status(400).json({ message: 'You dont have drivers rights' });
  }
};
