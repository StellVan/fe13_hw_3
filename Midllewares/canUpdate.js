const { truckModel } = require("../Models/models");

module.exports.canUpdate = (req, res, next) => {
  if (req.user.role !== "DRIVER") {
    next();
  } else if (req.user.role === "DRIVER") {
    truckModel
      .findOne({ assigned_to: req.user._id, status: "OL" })
      .exec()
      .then((data) => {
        if (!data) {
          next();
        } else if (data) {
          console.log(data);
          return res.status(400).json({
            message: "Driver is not able to change his data during load",
          });
        }
      });
  }
};
