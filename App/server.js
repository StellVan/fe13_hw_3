const express = require('express');
const app = express();
const { authRouter } = require('../Routers/authRouter');
const { userRouter } = require('../Routers/userRouter');
const { truckRouter } = require('../Routers/truckRouter');
const { loadRouter } = require('../Routers/loadRouter');
const { dbURI } = require('../config/config');
const mongoose = require('mongoose');

mongoose.connect(dbURI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true,
});

app.use(express.json());
app.use('/api/auth', authRouter);
app.use('/api/users', userRouter);
app.use('/api/trucks', truckRouter);
app.use('/api/loads', loadRouter);

module.exports = {
  app: app,
};
