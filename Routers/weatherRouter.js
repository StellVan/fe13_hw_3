const express = require('express');
const router = express.Router();
const { getWeather } = require('../Controllers/weatherController');
const { auth } = require('../Midllewares/auth');

router.get('/', auth, getWeather);

module.exports = {
  weatherRouter: router,
};
