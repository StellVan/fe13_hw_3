const express = require('express');
const router = express.Router();
const {
  addTruck,
  getTrucks,
  assignTruck,
  updateTruck,
  deleteTruck,
  getTrucksById,
} = require('../Controllers/truckController');
const { auth } = require('../Midllewares/auth');
const { isDriver } = require('../Midllewares/isDriver');

router.get('/', auth, isDriver, getTrucks);
router.post('/', auth, isDriver, addTruck);
router.get('/:id', auth, isDriver, getTrucksById);
router.delete('/:id', auth, isDriver, deleteTruck);
router.put('/:id', auth, isDriver, updateTruck);
router.post('/:id/assign', auth, isDriver, assignTruck);

module.exports = {
  truckRouter: router,
};
