const express = require('express');
const router = express.Router();
const {
  getProfileInfo,
  changePassword,
  deleteAccount,
} = require('../Controllers/userController');
const { auth } = require('../Midllewares/auth');
const { canUpdate } = require('../Midllewares/canUpdate');

router.get('/me', auth, getProfileInfo);
router.delete('/me', auth, canUpdate, deleteAccount);
router.patch('/me/password', auth, canUpdate, changePassword);

module.exports = {
  userRouter: router,
};
