const express = require('express');
const router = express.Router();
const {
  addLoad,
  getLoads,
  updateLoad,
  deleteLoad,
  postLoad,
  getAssignedLoads,
  updateStatus,
  getLoadById,
  getShippingInfo,
} = require('../Controllers/loadController');
const { auth } = require('../Midllewares/auth');
const { isShipper } = require('../Midllewares/isShipper');
const { isDriver } = require('../Midllewares/isDriver');

router.get('/active', auth, isDriver, getAssignedLoads);
router.patch('/active/state', auth, isDriver, updateStatus);

router.get('/', auth, getLoads);
router.get('/:id', auth, isShipper, getLoadById);
router.get('/:id/shipping_info', auth, isShipper, getShippingInfo);
router.post('/', auth, isShipper, addLoad);
router.put('/:id', auth, isShipper, updateLoad);
router.delete('/:id', auth, isShipper, deleteLoad);
router.post('/:id/post', auth, isShipper, postLoad);

module.exports = {
  loadRouter: router,
};
