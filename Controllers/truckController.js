const { truckModel } = require('../Models/models');
const { getFormatedDate } = require('../util/dateFormat');
const { truckTypes } = require('../util/truckTypes');

module.exports.addTruck = (req, res) => {
  const { _id } = req.user;
  const { type } = req.body;

  let truck = new truckModel({
    created_by: _id,
    assigned_to: 'not assigned',
    assigned_to_load: 'not assigned',
    status: 'OS',
    state: 'not assigned',
    type: type,
    created_date: getFormatedDate(),
  });

  if (!type) {
    return res.status(400).json({ message: 'Truck type was not provided' });
  }

  if (!truckTypes.find((el) => el.type === type)) {
    return res.status(400).json({ message: 'No such truck type' });
  }

  truck
    .save()
    .then(() => {
      return res.json({ message: 'Truck created successfully' });
    })
    .catch((err) => {
      return res.status(500).json({ message: err.message });
    });
};

module.exports.getTrucks = (req, res) => {
  const { _id } = req.user;

  truckModel
    .find({ created_by: _id })
    .exec()
    .then((data) => {
      return res.json({ trucks: data });
    })
    .catch((err) => {
      return res.status(500).json({ message: err.message });
    });
};

module.exports.getTrucksById = (req, res) => {
  const { _id } = req.user;
  const truckId = req.params.id;

  truckModel
    .findOne({ created_by: _id, _id: truckId })
    .exec()
    .then((data) => {
      return res.json({ truck: data });
    })
    .catch((err) => {
      return res.status(500).json({ message: err.message });
    });
};

module.exports.assignTruck = (req, res) => {
  const { _id } = req.user;
  const truckId = req.params.id;

  truckModel
    .find({ assigned_to: _id })
    .exec()
    .then((data) => {
      if (data.length > 0) {
        return res
          .status(400)
          .json({ message: 'Cannot assign multiple trucks to yourself' });
      } else if (data.length === 0) {
        truckModel
          .findOne({ created_by: _id, _id: truckId })
          .exec()
          .then((data) => {
            if (!data) {
              return res.status(400).json({ message: 'No such truck' });
            }
            if (data.assigned_to !== 'not assigned') {
              return res
                .status(400)
                .json({ message: 'Truck is already assigned' });
            } else if (data.assigned_to === 'not assigned') {
              truckModel
                .updateOne(
                  { created_by: _id, _id: truckId },
                  { assigned_to: _id, state: 'assigned', status: 'IS' }
                )
                .exec()
                .then(() => {
                  return res.json({ message: 'Truck assigned successfully' });
                })
                .catch((err) => {
                  return res.status(500).json({ message: err.message });
                });
            }
          })
          .catch((err) => {
            return res.status(500).json({ message: err.message });
          });
      }
    });
};

module.exports.updateTruck = (req, res) => {
  const truckId = req.params.id;
  const { _id } = req.user;
  const { type } = req.body;

  if (!type) {
    return res.status(400).json({ message: 'Truck type was not provided' });
  }

  truckModel
    .findOne({ created_by: _id, _id: truckId })
    .exec()
    .then((data) => {
      if (!data) {
        return res.status(400).json({ message: 'No such truck' });
      }
      if (data.assigned_to !== 'not assigned') {
        return res
          .status(400)
          .json({ message: 'Cannot update assigned truck' });
      } else if (data.status === 'OL') {
        return res.status(400).json({ message: 'Cannot update truck On Load' });
      } else if (data.assigned_to === 'not assigned') {
        if (!truckTypes.find((el) => el.type === type)) {
          return res.status(400).json({ message: 'No such truck type' });
        }

        truckModel
          .updateOne(
            { created_by: _id, _id: truckId },
            {
              type: type,
            }
          )
          .exec()
          .then(() => {
            return res.json({ message: 'Truck details changed successfully' });
          })
          .catch((err) => {
            return res.status(500).json({ message: err.message });
          });
      }
    })
    .catch((err) => {
      return res.status(500).json({ message: err.message });
    });
};

module.exports.deleteTruck = (req, res) => {
  const { _id } = req.user;
  const truckId = req.params.id;

  truckModel
    .findOne({ created_by: _id, _id: truckId })
    .exec()
    .then((data) => {
      if (!data) {
        return res.status(400).json({ message: 'No such truck' });
      }
      if (data.assigned_to !== 'not assigned') {
        return res
          .status(400)
          .json({ message: 'Cannot delete assigned truck' });
      } else if (data.status === 'OL') {
        return res.status(400).json({ message: 'Cannot delete truck On Load' });
      } else if (data.assigned_to === 'not assigned') {
        truckModel
          .deleteOne({ created_by: _id, _id: truckId })
          .exec()
          .then(() => {
            return res.json({ message: 'Truck deleted successfully' });
          })
          .catch((err) => {
            return res.status(500).json({ message: err.message });
          });
      }
    })
    .catch((err) => {
      return res.status(500).json({ message: err.message });
    });
};
