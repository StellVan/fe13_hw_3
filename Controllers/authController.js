const { credentialModel, userModel } = require("../Models/models");
const { secret } = require("../config/config");
const jwt = require("jsonwebtoken");
const { getFormatedDate } = require("../util/dateFormat");
const { generateNewPassword } = require("../util/passwordGenerator");
const { sendEmail } = require("../util/sendEmail");

module.exports.registerUser = (req, res) => {
  const { email, password, role } = req.body;

  if (!email && !password) {
    return res
      .status(400)
      .json({ message: "No email, password or/and role were provided" });
  }
  if (!email) {
    return res.status(400).json({ message: "No email was provided" });
  }
  if (!password) {
    return res.status(400).json({ message: "No password was provided" });
  }
  if (!role) {
    return res.status(400).json({ message: "No role was provided" });
  }
  if (role !== "DRIVER" && role !== "SHIPPER") {
    return res.status(400).json({ message: `No such role as ${role}` });
  }

  const cred = new credentialModel({
    email: email,
    password: password,
  });

  cred
    .save()
    .then(() => {
      const user = new userModel({
        email: email,
        role: role,
        created_date: getFormatedDate(),
      });

      user
        .save()
        .then(() => {
          res.json({ message: "Profile created successfully" });
        })
        .catch((err) => {
          res.status(500).json({ message: err.message });
        });
    })
    .catch((err) => {
      res.status(500).json({ message: err.message });
    });
};

module.exports.loginUser = (req, res) => {
  const { email, password } = req.body;

  if (!email && !password) {
    return res
      .status(400)
      .json({ message: "No email and no password were provided" });
  }
  if (!email) {
    return res.status(400).json({ message: "No email was provided" });
  }
  if (!password) {
    return res.status(400).json({ message: "No password was provided" });
  }

  credentialModel
    .findOne({ email })
    .exec()
    .then((user) => {
      if (!user) {
        return res.status(400).json({ message: "No such user" });
      } else if (user.password !== password) {
        return res.status(400).json({ message: "Wrong password" });
      } else {
        userModel
          .findOne({ email })
          .exec()
          .then((data) => {
            res
              .status(200)
              .json({ jwt_token: jwt.sign(JSON.stringify(data), secret) });
          })
          .catch(() => {
            res.status(500).json({ status: err.message });
          });
      }
    })
    .catch(() => {
      res.status(500).json({ status: err.message });
    });
};

module.exports.forgotPassword = (req, res) => {
  const userEmail = req.body.email;
  const newPassword = generateNewPassword();

  credentialModel
    .findOneAndUpdate({ email: userEmail }, { password: newPassword })
    .exec()
    .then((data) => {
      if (!data) {
        return res.json(400).json({ message: "No such user!" });
      } else {
        sendEmail(
          res,
          userEmail,
          "Password recovery (StellVan API EPAM)",
          `Hey there!
           It seems like you forgot your password.
           Here is new for you: ${newPassword}
          `,
          { message: "New password sent to your email address" }
        );
      }
    });
};
