const { loadModel, truckModel, userModel } = require("../Models/models");
const { getFormatedDate } = require("../util/dateFormat");
const { sendEmail } = require("../util/sendEmail");
const { truckTypes } = require("../util/truckTypes");

module.exports.addLoad = (req, res) => {
  const { _id } = req.user;
  const {
    dimensions,
    payload,
    name,
    pickup_address,
    delivery_address,
  } = req.body;

  if (!dimensions) {
    return res.status(400).json({ message: "No dimensions were provided" });
  }

  const { width, length, height } = dimensions;

  if (!width || !length || !height) {
    return res
      .status(400)
      .json({ message: "Not all required dimensions were provided" });
  }

  if (!payload) {
    return res.status(400).json({ message: "Payload was not provided" });
  }
  if (!name) {
    return res.status(400).json({ message: "Name was not provided" });
  }
  if (!pickup_address) {
    return res.status(400).json({ message: "Pickup address was not provided" });
  }
  if (!delivery_address) {
    return res
      .status(400)
      .json({ message: "Delivery address was not provided" });
  }

  let load = new loadModel({
    created_by: _id,
    assigned_to: "not assigned",
    assigned_to_truck: "not assigned",
    status: "NEW",
    state: "not assigned",
    name: name,
    payload: payload,
    pickup_address: pickup_address,
    delivery_address: delivery_address,
    dimensions: dimensions,
    logs: [{ message: "Load created", time: getFormatedDate() }],
    created_date: getFormatedDate(),
  });

  load
    .save()
    .then(() => {
      return res.json({ message: "Load created successfully" });
    })
    .catch((err) => {
      return res.status(500).json({ message: err.message });
    });
};

module.exports.getLoads = (req, res) => {
  const { _id } = req.user;
  const { role } = req.user;
  const { status, limit, offset } = req.query;

  if (role === "SHIPPER") {
    let searchArray;
    if (status === "ALL" || !status) {
      searchArray = [
        { status: "NEW" },
        { status: "POSTED" },
        { status: "ASSIGNED" },
        { status: "SHIPPED" },
      ];
    } else {
      searchArray = [{ status: status }];
    }
    loadModel
      .find({
        created_by: _id,
        $or: searchArray,
      })
      .exec()
      .then((data) => {
        let result = [];

        for (
          let i = +(limit || 10) * +(offset || 0);
          i < +(limit || 10) + +(limit || 10) * +(offset || 0);
          i++
        ) {
          if (data[i]) {
            result.push(data[i]);
          }
        }

        return res.json({ loads: result });
      })
      .catch((err) => {
        return res.status(500).json({ message: err.message });
      });
  } else if (role === "DRIVER") {
    let searchArray;
    if (status === "NEW" || status === "POSTED") {
      return res
        .status(400)
        .json({ message: "You dont have rights to view this" });
    }
    if (status === "ALL" || !status) {
      searchArray = [{ status: "SHIPPED" }, { status: "ASSIGNED" }];
    } else {
      searchArray = [{ status: status }];
    }
    loadModel
      .find({
        assigned_to: _id,
        $or: searchArray,
      })
      .exec()
      .then((data) => {
        let result = [];

        for (
          let i = (+limit || 10) * (+offset || 0);
          i < (+limit || 10) + (+limit || 10) * (+offset || 0);
          i++
        ) {
          if (data[i]) {
            result.push(data[i]);
          }
        }

        return res.json({ loads: result });
      })
      .catch((err) => {
        return res.status(500).json({ message: err.message });
      });
  }
};

module.exports.getLoadById = (req, res) => {
  const { _id } = req.user;
  const loadId = req.params.id;

  loadModel
    .findOne({ created_by: _id, _id: loadId })
    .exec()
    .then((data) => {
      if (!data) {
        return res.status(400).json({ message: "No such load" });
      }
      return res.json({ load: data });
    })
    .catch((err) => {
      return res.status(500).json({ message: err.message });
    });
};

module.exports.getShippingInfo = (req, res) => {
  const { _id } = req.user;
  const loadId = req.params.id;

  loadModel
    .findOne({ created_by: _id, _id: loadId })
    .exec()
    .then((data) => {
      if (!data) {
        return res.status(400).json({ message: "No such load" });
      }
      if (data.status === "NEW" || data.status === "POSTED") {
        return res
          .status(400)
          .json({ message: "New loads do not have shipping info" });
      }
      return res.json({ load: data });
    })
    .catch((err) => {
      return res.status(500).json({ message: err.message });
    });
};

module.exports.updateLoad = (req, res) => {
  const loadId = req.params.id;
  const { _id } = req.user;
  const {
    dimensions,
    payload,
    name,
    pickup_address,
    delivery_address,
  } = req.body;

  if (!dimensions) {
    return res.status(400).json({ message: "No dimensions were provided" });
  }

  const { width, length, height } = dimensions;

  if (!width || !length || !height) {
    return res
      .status(400)
      .json({ message: "Not all required dimensions were provided" });
  }

  if (!payload) {
    return res.status(400).json({ message: "Payload was not provided" });
  }
  if (!name) {
    return res.status(400).json({ message: "Name was not provided" });
  }
  if (!pickup_address) {
    return res.status(400).json({ message: "Pickup address was not provided" });
  }
  if (!delivery_address) {
    return res
      .status(400)
      .json({ message: "Delivery address was not provided" });
  }

  loadModel
    .findOne({ created_by: _id, _id: loadId })
    .exec()
    .then((data) => {
      if (!data) {
        return res.status(400).json({ message: "No such load" });
      }
      if (data.status !== "NEW") {
        return res.status(400).json({ message: "Can update only NEW loads" });
      } else if (data.status === "NEW") {
        loadModel
          .updateOne(
            { created_by: _id, _id: loadId },
            {
              dimensions: dimensions,
              payload: payload,
              name: name,
              payload: payload,
              pickup_address: pickup_address,
              delivery_address: delivery_address,
              dimensions: dimensions,
              $push: {
                logs: { message: "Load updated", time: getFormatedDate() },
              },
            }
          )
          .exec()
          .then(() => {
            return res.json({ message: "Load details changed successfully" });
          })
          .catch((err) => {
            return res.status(500).json({ message: err.message });
          });
      }
    })
    .catch((err) => {
      return res.status(500).json({ message: err.message });
    });
};

module.exports.deleteLoad = (req, res) => {
  const { _id } = req.user;
  const loadId = req.params.id;

  loadModel
    .findOne({ created_by: _id, _id: loadId })
    .exec()
    .then((data) => {
      if (!data) {
        return res.status(400).json({ message: "No such load" });
      }
      if (data.status !== "NEW") {
        return res.status(400).json({ message: "Can delete only NEW loads" });
      } else if (data.status === "NEW") {
        loadModel
          .deleteOne({ created_by: _id, _id: loadId })
          .exec()
          .then(() => {
            return res.json({ message: "Load deleted successfully" });
          })
          .catch((err) => {
            return res.status(500).json({ message: err.message });
          });
      }
    })
    .catch((err) => {
      return res.status(500).json({ message: err.message });
    });
};

module.exports.postLoad = (req, res) => {
  const shipperId = req.user._id;
  const loadId = req.params.id;

  loadModel
    .findOne({ created_by: shipperId, _id: loadId })
    .exec()
    .then((loadData) => {
      if (!loadData) {
        return res.status(400).json({ message: "No such load" });
      }
      if (loadData.status !== "NEW") {
        return res.status(400).json({ message: "Can post only NEW loads" });
      } else if (loadData.status === "NEW") {
        loadModel
          .updateOne(
            { created_by: shipperId, _id: loadId },
            {
              status: "POSTED",
              $push: {
                logs: { message: "Load posted", time: getFormatedDate() },
              },
            }
          )
          .exec()
          .then(() => {
            truckModel
              .find({
                status: "IS",
                state: "assigned",
              })
              .exec()
              .then((truckData) => {
                const suitableTrucks = [];

                for (let i = 0; i < truckData.length; i++) {
                  let tempTruck = truckTypes.find(
                    (element) => element.type === truckData[i].type
                  );

                  if (
                    tempTruck.dimensions.width > loadData.dimensions.width &&
                    tempTruck.dimensions.length > loadData.dimensions.length &&
                    tempTruck.dimensions.height > loadData.dimensions.height &&
                    tempTruck.payload > loadData.payload
                  ) {
                    suitableTrucks.push(truckData[i]);
                  }
                }

                if (suitableTrucks.length !== 0) {
                  truckModel
                    .updateOne(
                      {
                        created_by: suitableTrucks[0].created_by,
                        _id: suitableTrucks[0]._id,
                      },
                      { status: "OL", assigned_to_load: loadData._id }
                    )
                    .exec()
                    .then(() => {
                      loadModel
                        .updateOne(
                          { created_by: shipperId, _id: loadId },
                          {
                            status: "ASSIGNED",
                            state: "En route to Pick Up",
                            assigned_to: suitableTrucks[0].created_by,
                            assigned_to_truck: suitableTrucks[0]._id,
                            $push: {
                              logs: {
                                message: `Load assigned to driver with id ${suitableTrucks[0].created_by}`,
                                time: getFormatedDate(),
                              },
                            },
                          }
                        )
                        .exec()
                        .then(() => {
                          return res.json({
                            message: "Load posted successfully",
                            driver_found: true,
                          });
                        })
                        .catch((err) => {
                          return res.status(500).json({ message: err.message });
                        });
                    })
                    .catch((err) => {
                      return res.status(500).json({ message: err.message });
                    });
                } else if (suitableTrucks.length === 0) {
                  loadModel
                    .updateOne(
                      { created_by: shipperId, _id: loadId },
                      {
                        status: "NEW",
                        $push: {
                          logs: {
                            message: "Load post denied, driver was not found",
                            time: getFormatedDate(),
                          },
                        },
                      }
                    )
                    .exec()
                    .then(() => {
                      return res.json({
                        message: "Cannot find suitable truck",
                        driver_found: false,
                      });
                    })
                    .catch((err) => {
                      return res.status(500).json({ message: err.message });
                    });
                }
              })
              .catch((err) => {
                return res.status(500).json({ message: err.message });
              });
          })
          .catch((err) => {
            return res.status(500).json({ message: err.message });
          });
      }
    })
    .catch((err) => {
      return res.status(500).json({ message: err.message });
    });
};

module.exports.getAssignedLoads = (req, res) => {
  const { _id } = req.user;

  loadModel
    .findOne({ assigned_to: _id, status: "ASSIGNED" })
    .exec()
    .then((loadData) => {
      return res.json({ load: loadData });
    })
    .catch((err) => {
      return res.status(500).json({ message: err.message });
    });
};

module.exports.updateStatus = (req, res) => {
  const { _id } = req.user;

  loadModel
    .findOne({
      assigned_to: _id,
      status: "ASSIGNED",
    })
    .exec()
    .then((loadData) => {
      if (!loadData) {
        return res.status(400).json({ message: "No such load" });
      }
      if (loadData.status !== "ASSIGNED") {
        return res
          .status(400)
          .json({ message: "Cannot change status on non-assigned load" });
      } else {
        userModel
          .findOne({ _id: loadData.created_by })
          .exec()
          .then((shipperData) => {
            if (loadData.state === "En route to Pick Up") {
              loadModel
                .updateOne(
                  { assigned_to: _id, status: "ASSIGNED" },
                  {
                    state: "En route to Delivery",
                    $push: {
                      logs: {
                        message: "Load state updated to 'En route to Delivery'",
                        time: getFormatedDate(),
                      },
                    },
                  }
                )
                .exec()
                .then(() => {
                  sendEmail(
                    res,
                    shipperData.email,
                    "Load status has been updated (StellVan API EPAM)",
                    `Your load "${loadData.name}" with id: ${loadData._id} is en route to delivery`,
                    {
                      message: "Load state changed to 'En route to Delivery'",
                    }
                  );
                })
                .catch((err) => {
                  return res.status(500).json({ message: err.message });
                });
            } else if (loadData.state === "En route to Delivery") {
              loadModel
                .updateOne(
                  { assigned_to: _id, status: "ASSIGNED" },
                  {
                    state: "Arrived to delivery",
                    status: "SHIPPED",
                    $push: {
                      logs: {
                        message: 'Load state updated to "Arrived to delivery"',
                        time: getFormatedDate(),
                      },
                    },
                  }
                )
                .exec()
                .then(() => {
                  truckModel
                    .updateOne(
                      { assigned_to: _id },
                      {
                        status: "OS",
                        assigned_to_load: "not assigned",
                        assigned_to: "not assigned",
                        state: "not assigned",
                      }
                    )
                    .exec()
                    .then(() => {
                      sendEmail(
                        res,
                        shipperData.email,
                        "Load status has been updated (StellVan API EPAM)",
                        `Your load "${loadData.name}" with id: ${loadData._id} Arrived to delivery`,
                        {
                          message:
                            "Load state changed to 'Arrived to delivery'",
                        }
                      );
                    })
                    .catch((err) => {
                      return res.status(500).json({ message: err.message });
                    });
                })
                .catch((err) => {
                  return res.status(500).json({ message: err.message });
                });
            }
          })
          .catch((err) => {
            return res.status(500).json({ message: err.message });
          });
      }
    })
    .catch((err) => {
      return res.status(500).json({ message: err.message });
    });
};
