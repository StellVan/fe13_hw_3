const {
  userModel,
  credentialModel,
  truckModel,
  loadModel,
} = require('../Models/models');

module.exports.getProfileInfo = (req, res) => {
  const userId = req.user._id;

  userModel
    .findOne({ _id: userId })
    .exec()
    .then((data) => {
      if (!data) {
        return res.status(400).json({ message: 'No such user' });
      }
      res.json({
        user: data,
      });
    })
    .catch((err) => {
      res.status(500).json({ message: err.message });
    });
};

module.exports.changePassword = (req, res) => {
  const email = req.user.email;
  const { oldPassword, newPassword } = req.body;

  if (!oldPassword && !newPassword) {
    return res.status(400).json({ message: 'Passwords were not provided' });
  }

  if (!oldPassword) {
    return res.status(400).json({ message: 'Old password was not provided' });
  }

  if (!newPassword) {
    return res.status(400).json({ message: 'New password was not provided' });
  }

  credentialModel
    .findOne({ email: email })
    .exec()
    .then((data) => {
      if (oldPassword === data.password) {
        credentialModel
          .updateOne({ email: email }, { password: newPassword })
          .exec()
          .then(() => {
            res.json({ message: 'Password changed successfully' });
          })
          .catch((err) => {
            return res.status(500).json({ message: err.message });
          });
      } else {
        return res.status(400).json({ message: 'Wrong old password!' });
      }
    })
    .catch((err) => {
      return res.status(500).json({ message: err.message });
    });
};

module.exports.deleteAccount = (req, res) => {
  const { email, _id } = req.user;

  userModel
    .findOneAndDelete({ email: email })
    .exec()
    .then((data) => {
      if (!data) {
        return res.status(400).json({ message: 'No such user' });
      } else {
        truckModel
          .deleteMany({ created_by: _id })
          .exec()
          .then(() => {
            loadModel
              .deleteMany({ created_by: _id })
              .exec()
              .then(() => {
                credentialModel
                  .findOneAndDelete({ email: email })
                  .exec()
                  .then(() => {
                    res.json({ message: 'Profile deleted successfully' });
                  })
                  .catch((err) => {
                    return res.status(500).json({ message: err.message });
                  });
              })
              .catch((err) => {
                return res.status(500).json({ message: err.message });
              });
          })
          .catch((err) => {
            return res.status(500).json({ message: err.message });
          });
      }
    })
    .catch((err) => {
      return res.status(500).json({ message: err.message });
    });
};
